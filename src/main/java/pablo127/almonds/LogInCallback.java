package pablo127.almonds;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public interface LogInCallback {
	public void done(ParseUser obj, ParseException e);
}
