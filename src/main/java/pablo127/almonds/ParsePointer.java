package pablo127.almonds;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * 
 * @author jskrepnek (<a href="https://bitbucket.org/jskrepnek" target="_blank">bitbucket page</a>)
 */
public class ParsePointer extends JSONObject {
	public ParsePointer(String className, String objectId) 	{
		try {
			this.put("__type", "Pointer");
			this.put("className", className);
			this.put("objectId", objectId);
		} catch (JSONException e) {

		}
	}
}
