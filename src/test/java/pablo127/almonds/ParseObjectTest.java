package pablo127.almonds;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseObjectTest {

	@Before
	public void setUp() throws Exception {
		Parse.initialize("B3jrmh2W9yIzYCjOLHLI5tLcApvFl1NJAoiJMnTP", "eHDcUJ9DJJqrIsccHbBUFjb0d3srvtnbAJoQxhLL");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddObject() {
		ParseObject object = ParseObject.create("test");
		object.put("x", "test value");
		object.put("y", "y val");
		try {
			object.save();
		} catch (ParseException e1) {
			e1.printStackTrace();
			assertEquals(null, e1);
		}
		try {
			object.delete();
		} catch (ParseException e) {
			assertNull(e);
		}
	}
	
	@Test
	public void testSavingDate() {
		ParseObject obj = new ParseObject("daty");
		obj.put("date", Calendar.getInstance().getTime());
		obj.save(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				assertEquals(null, e);
			}
		});
		try {
			obj.delete();
		} catch (ParseException e1) {
			assertNull(e1);
		}
	}
	
	@Test
	public void testGetters(){
		ParseQuery q = new ParseQuery("getters");
		q.find(new FindCallback() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				assertEquals(null, e);
				assertEquals(1, objects.size());
				ParseObject obj = objects.get(0);
				
				assertEquals(new Integer(9), obj.getInt("int"));
				assertEquals(new Double(1.1111), obj.getDouble("float"));
				assertEquals(null, obj.getDouble("int"));
				assertEquals(null, obj.getInt("double"));
				assertEquals(new Double(2.2222), obj.getDouble("double"));					
				assertEquals(null, obj.getInt("float"));
				
				assertEquals("string", obj.getString("string"));				
				
			}
		});
	}
	
	@Test
	public void testAddModificateObject(){
		ParseObject test = ParseObject.create("GameScore");
		test.put("score", 20);
		test.save(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				assertEquals(null, e);
				ParseQuery q = new ParseQuery("GameScore");
				q.find(new FindCallback() {
					
					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						assertEquals(null, e);
						assertEquals(1, objects.size());
						assertEquals(20, objects.get(0).get("score"));
						objects.get(0).put("score", 30);
						assertEquals(30, objects.get(0).get("score"));
						objects.get(0).save(new SaveCallback() {
							
							@Override
							public void done(ParseException e) {
								assertEquals(null, e);
								ParseQuery q = new ParseQuery("GameScore");
								q.find(new FindCallback() {
									
									@Override
									public void done(List<ParseObject> objects, ParseException e) {
										assertEquals(null, e);
										assertEquals(1, objects.size());
										assertEquals(30, objects.get(0).get("score"));
										try {
											objects.get(0).delete();
										} catch (ParseException e1) {
											assertNotNull(e1);
										}
									}
								});
								
							}
						});
					}
				});
			}
		});
	}

}
