REST and Apache Http Components based Java Parse API that mimics the Android and IOS APIs.

For detailed instructions, see documentation of the Parse Android API at http://parse.com.

It's got sending, receiving data, triggering functions and much more.

[JavaDocs are here](http://cv-pabloo.rhcloud.com/almonds-javadoc/)

To add almonds to your project use maven, gradle or other building tool.
### Maven ###
To use it in maven, add to your pom.xml
in repositories section:
```
#!xml
<repositories>
        ...
	<repository>
		<id>almonds-repository</id>
		<url>https://bitbucket.org/pablo127/almonds/raw/master/repository/</url>
	</repository>
        ...
</repositories>
```

in dependencies section:

```
#!xml

<dependencies>
        ...
	<dependency>
		<groupId>pablo127</groupId>
		<artifactId>almonds</artifactId>
		<version>1.0.1</version>
	</dependency>
        ...
</dependencies>
```
### Gradle ###
For android add under *apply plugin* :
```
#!json
apply plugin: 'com.android.application'
repositories {
    maven {
    	...
        url "https://bitbucket.org/pablo127/almonds/raw/master/repository/"
        ...
    }
}
...
```
add in dependencies
```
#!json

dependencies {
    ...
    compile group: 'pablo127', name: 'almonds', version: '1.0.1'
    ...
}
```


### Manual ###
If you don't use building system, you can just download latest jar from https://bitbucket.org/pablo127/almonds/raw/master/repository


### Problems ###
[Write an issue.](https://bitbucket.org/pablo127/almonds/issues/new)